import axios from 'axios';

export async function login(uid) {
	try {
		return await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/getUser/${uid}`);
	} catch (error) {
		return error;
	}
}

export async function register(Userdata) {
	try {
		return await axios.post(`${process.env.REACT_APP_API_URL}/api/v1/newUser`, {
			...Userdata,
		});
	} catch (error) {
		return error;
	}
}

export async function sendEmail(email) {
	try {
		return await axios.post(`${process.env.REACT_APP_API_URL}/api/v1/sendMailRegister`, {
			email,
		});
	} catch (error) {
		return error;
	}
}

export async function getValidConfirmation(token) {
	try {
		return await axios.post(`${process.env.REACT_APP_API_URL}/api/v1/tokenValidation`, {
			token,
		});
	} catch (error) {
		return error;
	}
}
