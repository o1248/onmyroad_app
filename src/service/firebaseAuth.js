/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from 'react';
import { useDispatch } from 'react-redux';
import { GoogleAuthProvider, FacebookAuthProvider } from 'firebase/auth';
import {
	auth,
	authUI,
} from '../firebase/firestore';
import { profileLogin } from '../store/profile/profileSlice';
import { login, register } from './api';

import './auth.scss';

async function reSubInApp(user) {
	try {
		return await register({
			uid: user.uid,
			email: user.email,
			displayName: user.displayName,
		});
	} catch (error) {
		return error;
	}
}

function AuthAuthentification() {
	const dispatch = useDispatch();
	const [user, setUser] = React.useState(null);

	async function authentification(currentUser) {
		const userId = currentUser.user.uid;
		await reSubInApp(currentUser.user);

		const usersProfile = await login(userId);
		dispatch(profileLogin({ profile: usersProfile.data }));
	}

	React.useEffect(() => {
		authUI.start('.google-login', {
			signInOptions: [FacebookAuthProvider.PROVIDER_ID, GoogleAuthProvider.PROVIDER_ID],
			signInFlow: 'popup',
			callbacks: {
				signInSuccessWithAuthResult(currentUser, credential, redirectUrl) {
					authentification(currentUser);

					return false;
				},
			},
		});
	}, []);

	return <div className="google-login" />;
}

export default AuthAuthentification;
