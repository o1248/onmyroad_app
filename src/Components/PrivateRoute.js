/* eslint-disable import/prefer-default-export */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React from 'react';
import { Outlet, Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import {
	IsConnected,
} from '../store/profile/profileSlice';

export function PrivateRoute() {
	const isConnected = useSelector(IsConnected);

	return isConnected ? <Outlet /> : <Navigate to="/login" />;
}
