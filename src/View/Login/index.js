import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import FirebaseAuth from '../../service/firebaseAuth';

import {
	authEmail,
} from '../../firebase/firestore';
import { profileLogin } from '../../store/profile/profileSlice';

import Icon from '../../Assets/mini_Icon.png';
import './index.scss';

export default function Login() {
	const [loginEmail, setLoginEmail] = React.useState('antony.jacques06@gmail.com');
	const [loginPassword, setLoginPassword] = React.useState('');

	const dispatch = useDispatch();

	async function authenticateUser(email, password) {
		try {
			authEmail(email, password).then((userProfile) => dispatch(profileLogin({ profile: userProfile.data })));
		} catch (err) {
			console.log(err);
		}
	}

	return (
		<div id="login" className="auth-form">
			<div className="auth-form__header">
				<div><Link to="/"><img src={Icon} alt="icon" /></Link></div>
				<h1 className="current">Connexion</h1>
			</div>
			<div className="auth-form__wrapper">
				<div className="auth-form__wrapper__section">
					<FirebaseAuth />
				</div>
				<div className="auth-form__wrapper__section">
					<form onSubmit={(e) => e.preventDefault()} className="form">
						<div className="auth-form__wrapper__section__fields">
							<label htmlFor="loginEmail" className="form__labels">
								<span>Email</span>
								<br />
								<input
									placeholder="Email Address"
									name="loginEmail"
									id="loginEmail"
									type="email"
									value={loginEmail || ''}
									onChange={(e) => setLoginEmail(e.target.value)}
								/>
							</label>
						</div>
						<div className="auth-form__wrapper__section__fields">
							<label htmlFor="loginPassword" className="form__labels">
								<span>Mot de passe</span>
								<br />
								<input
									placeholder="Password"
									name="loginPassword"
									id="loginPassword"
									type="password"
									value={loginPassword || ''}
									onChange={(e) => setLoginPassword(e.target.value)}
								/>
								<span className="forgot_password"><a href="/retrieve-password">Mot de passe oublié ?</a></span>
							</label>
						</div>
						<button
							type="button"
							onClick={() => authenticateUser(loginEmail, loginPassword, true)}
							className="auth-form__wrapper__section__buttons"
						>
							Connexion
						</button>
					</form>
				</div>
				<div className="auth-form__wrapper__section">
					<div><Link to="/">← Revenir à la page principale</Link></div>
				</div>
			</div>
		</div>
	);
}
