/* eslint-disable default-case */
/* eslint-disable react/no-this-in-sfc */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react';
import { Link, useParams } from 'react-router-dom';

import Icon from '../../Assets/mini_Icon.png';
import './index.scss';

export default function Confirmation() {
	const { message } = useParams();

	const [messageUser, setMessageUser] = React.useState('');

	React.useEffect(() => {
		switch (message) {
		case 'email':
			setMessageUser('Un email à été envoyé à votre addresse mail pour la confirmation de votre inscription');
			break;
		case 'subscribe':
			setMessageUser('Votre compte à bien été créer, vous pouvez dès à présent vous connectez avec vos identifiants');
			break;
		}
	}, []);

	return (
		<div id="login" className="auth-form">
			<div className="auth-form__header">
				<div><Link to="/"><img src={Icon} alt="icon" /></Link></div>
				<h1 className="current">Confirmation</h1>
			</div>
			<div className="auth-form__wrapper">
				<div className="auth-form__wrapper__section">
					<div className="auth-form__wrapper__section__fields">
						{ messageUser }
					</div>
				</div>
			</div>
			<div className="auth-form__wrapper__section">
				<div><Link to="/">← Revenir à la page principale</Link></div>
			</div>
		</div>
	);
}
