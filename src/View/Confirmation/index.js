/* eslint-disable default-case */
/* eslint-disable react/no-this-in-sfc */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react';
import { Link, useParams, useNavigate } from 'react-router-dom';

import {
	ConfirmEmail,
} from '../../firebase/firestore';

import {
	getValidConfirmation,
} from '../../service/api';

import Icon from '../../Assets/mini_Icon.png';
import './index.scss';

export default function Confirmation() {
	const { token } = useParams();
	const navigate = useNavigate();

	const [signupEmail, setSignupEmail] = React.useState('antony.jacques06@gmail.com');
	const [signupPassword, setSignupPassword] = React.useState('');

	const [errorFirebase, setErrorFirebase] = React.useState('');
	const [confirmationMessage, setConfirmationMessage] = React.useState('');

	async function checkToken() {
		if (token) {
			const goodToken = await getValidConfirmation(token);

			if (goodToken.status === 200) {
				setConfirmationMessage(true);
			} else {
				setConfirmationMessage(goodToken.response.statusText);
			}
		}
	}

	async function registerUser(email, password) {
		try {
			const success = await ConfirmEmail(email, password);
			if (success.status === 200) { navigate('/step/subscribe'); }
		} catch (error) {
			switch (error.code) {
			case 'auth/email-already-in-use':
				setErrorFirebase('Email déjà utilisé');
				break;
			case 'auth/weak-password':
				setErrorFirebase('Mot de passe trop faible');
				break;
			case 'password_missing_upercase':
				setErrorFirebase('Le mot de passe ne contient pas de majuscule');
				break;
			case 'password_missing_special_char':
				setErrorFirebase('Le mot de passe ne contient pas de caractère spécial');
				break;
			case 'password_missing_numeric':
				setErrorFirebase('Le mot de passe ne contient pas de chiffre');
				break;
			case 'password_missing_lowercase':
				setErrorFirebase('Le mot de passe ne contient pas de minuscule');
				break;
			case 'password_missing_more':
				setErrorFirebase('Le mot de passe ne contient pas au moins 8 caractères');
				break;
			}
		}
	}

	React.useEffect(() => {
		checkToken();
		setSignupEmail(window.localStorage.getItem('emailForSignIn'));
	}, []);

	return (
		<div id="login" className="auth-form">
			<div className="auth-form__header">
				<div><Link to="/"><img src={Icon} alt="icon" /></Link></div>
				<h1 className="current">Confirmation</h1>
			</div>
			{
				token && confirmationMessage === true
					? (
						<div className="auth-form__wrapper">
							<div className="auth-form__wrapper__section">
								<form onSubmit={(e) => e.preventDefault()} className="form">
									<div className="auth-form__wrapper__section__fields">
										<label htmlFor="loginEmail" className="form__labels">
											<span>Email</span>
											<br />
											<input
												disabled
												placeholder="Email Address"
												name="loginEmail"
												id="loginEmail"
												type="email"
												value={signupEmail || ''}
											/>
										</label>
									</div>
									<div className="auth-form__wrapper__section__fields">
										<label htmlFor="loginPassword" className="form__labels">
											<span>Mot de passe</span>
											<br />
											<input
												placeholder="Password"
												name="loginPassword"
												id="loginPassword"
												type="password"
												value={signupPassword || ''}
												onChange={(e) => setSignupPassword(e.target.value)}
											/>
										</label>
										<span className="error_fields">{errorFirebase}</span>
									</div>
									<button
										type="button"
										onClick={() => registerUser(signupEmail, signupPassword, true)}
										className="auth-form__wrapper__section__buttons"
									>
										Confirmation Inscription
									</button>
								</form>
							</div>
							<div className="auth-form__wrapper__section">
								<div><Link to="/">← Revenir à la page principale</Link></div>
							</div>
						</div>
					)
					: (
						<div className="auth-form__wrapper">
							<div className="auth-form__wrapper__section">
								<div className="auth-form__wrapper__section__fields">
									Votre demande ne peux pas aboutir
									<br />
									{ confirmationMessage }
								</div>
							</div>
							<div className="auth-form__wrapper__section">
								<div><Link to="/">← Revenir à la page principale</Link></div>
							</div>
						</div>
					)
			}
		</div>
	);
}
