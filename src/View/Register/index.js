/* eslint-disable no-unused-vars */
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import FirebaseAuth from '../../service/firebaseAuth';

import {
	signEmail,
} from '../../firebase/firestore';

import Icon from '../../Assets/mini_Icon.png';
import './index.scss';

export default function Register() {
	const [signupEmail, setSignupEmail] = React.useState('antony.jacques06@gmail.com');
	const [successSignUp, setSuccessSignUp] = React.useState(null);

	const navigate = useNavigate();

	async function registerUser(email, password) {
		try {
			const success = await signEmail(email, password);
			setSuccessSignUp(success);
		} catch (err) {
			console.log(err);
		}
	}

	React.useEffect(() => {
		if (successSignUp === true) { navigate('/step/email'); }
	}, [successSignUp]);

	return (
		<div id="login" className="auth-form">
			<div className="auth-form__header">
				<div><Link to="/"><img src={Icon} alt="icon" /></Link></div>
				<h1 className="current">Inscription</h1>
			</div>
			<div className="auth-form__wrapper">
				<div className="auth-form__wrapper__section">
					<FirebaseAuth />
				</div>
				<div className="auth-form__wrapper__section">
					<form onSubmit={(e) => e.preventDefault()} className="form">
						<div className="auth-form__wrapper__section__fields">
							<label htmlFor="loginEmail" className="form__labels">
								<span>Email</span>
								<br />
								<input
									placeholder="Email Address"
									name="loginEmail"
									id="loginEmail"
									type="email"
									value={signupEmail || ''}
									onChange={(e) => setSignupEmail(e.target.value)}
								/>
								<span className="error_fields">{successSignUp && successSignUp.response ? successSignUp.response.statusText : ''}</span>
							</label>
						</div>
						<button
							type="button"
							onClick={() => registerUser(signupEmail, true)}
							className="auth-form__wrapper__section__buttons"
						>
							Inscription
						</button>
					</form>
				</div>
				<div className="auth-form__wrapper__section">
					<div><Link to="/">← Revenir à la page principale</Link></div>
				</div>
			</div>
		</div>
	);
}
