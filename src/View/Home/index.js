/* eslint-disable no-nested-ternary */
/* eslint-disable react/button-has-type */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-undef */
import React from 'react';

import { useDispatch, useSelector } from 'react-redux';

import {
	IsConnected,
	profileLogout,
} from '../../store/profile/profileSlice';

import {
	auth,
} from '../../firebase/firestore';

export default function Home() {
	const isConnected = useSelector(IsConnected);
	const dispatch = useDispatch();

	const signOut = () => {
		auth.signOut().then(() => {
			dispatch(profileLogout());
		});
	};

	return (
		<div>
			home
			{
				isConnected
					? <button onClick={() => signOut()}>Log Out</button>
					: ''
			}
		</div>
	);
}
