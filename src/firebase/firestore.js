/* eslint-disable no-throw-literal */
/* eslint-disable no-unused-vars */
/* eslint-disable no-return-await */
/* eslint-disable no-return-assign */
/* eslint-disable max-len */
import { initializeApp } from 'firebase/app';
import {
	getAuth,
	createUserWithEmailAndPassword,
	signInWithEmailAndPassword,
	sendSignInLinkToEmail,
	// deleteUser,
} from 'firebase/auth';
import * as firebaseui from 'firebaseui';
import { login, register, sendEmail } from '../service/api';

// TODO: Replace the following with your app's Firebase project configuration
// See: https://firebase.google.com/docs/web/learn-more#config-object
const firebaseConfig = {
	apiKey: 'AIzaSyBjNlmXy-yU6nAgg8qXCAnvhT3Gi1j4RYs',
	authDomain: 'onmyroad-348c8.firebaseapp.com',
	projectId: 'onmyroad-348c8',
	storageBucket: 'onmyroad-348c8.appspot.com',
	messagingSenderId: '772746843119',
	appId: '1:772746843119:web:a4ac0a80a5e4f120a95c1a',
	measurementId: 'G-4W7ZGMN5R7',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
export const auth = getAuth(app);
auth.languageCode = 'fr';

export async function authEmail(email, password) {
	return signInWithEmailAndPassword(auth, email, password).then(async (response) => {
		try {
			await register({
				uid: response.user.uid,
				email: response.user.email,
				displayName: response.user.displayName,
			});

			const profile = await login(response.user.uid);
			return profile;
		} catch (error) {
			console.log(error);
			return error;
		}
	});
}
export async function signEmail(email) {
	const emailSuccess = await sendEmail(email);
	if (emailSuccess.status === 200) {
		window.localStorage.setItem('emailForSignIn', email);
		return true;
	}

	return emailSuccess;
}
export async function ConfirmEmail(email, password) {
	const Error = {};

	if (!password.match(/(.*[A-Z])/g)) {
		Error.code = 'password_missing_upercase';
		throw Error;
	}
	if (!password.match(/(.*[!@#$&?*,;:.%])/g)) {
		Error.code = 'password_missing_special_char';
		throw Error;
	}
	if (!password.match(/(.*[0-9])/g)) {
		Error.code = 'password_missing_numeric';
		throw Error;
	}
	if (!password.match(/(.*[a-z])/g)) {
		Error.code = 'password_missing_lowercase';
		throw Error;
	}
	if (!password.match(/^.{8,}$/g)) {
		Error.code = 'password_missing_more';
		throw Error;
	}

	return createUserWithEmailAndPassword(auth, email, password).then(async (response) => await register({
		uid: response.user.uid,
		email: response.user.email,
		displayName: response.user.displayName,
	}));
}
// export async function deleteAccount(user) { return deleteUser(user); }

export const authUI = new firebaseui.auth.AuthUI(auth);
