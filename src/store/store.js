/* eslint-disable import/prefer-default-export */
import { configureStore } from '@reduxjs/toolkit';
import storage from 'redux-persist/lib/storage';
import {
	persistReducer, FLUSH,
	REHYDRATE,
	PAUSE,
	PERSIST,
	PURGE,
	REGISTER,
} from 'redux-persist';

import profile from './profile/profileSlice';

const persistConfig = {
	key: 'root',
	storage,
};

const persistedReducerProfile = persistReducer(persistConfig, profile);

export const store = configureStore({
	reducer: {
		profile: persistedReducerProfile,
	},
	middleware: (getDefaultMiddleware) => getDefaultMiddleware({
		serializableCheck: {
			ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
		},
	}),
});
