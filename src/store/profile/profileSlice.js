/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	user: null,
	isConnected: false,
	errorInitProfile: null,
};

export const profileSlice = createSlice({
	name: 'profile',
	initialState,
	reducers: {
		profileLogin: (state, action) => {
			state.user = action.payload.profile;
			state.isConnected = true;
			// state.errorInitProfile = action.payload.error;
		},
		profileLogout: (state) => {
			state.user = null;
			state.isConnected = false;
			state.errorInitProfile = null;
		},
	},
});

export const { profileLogin, profileLogout } = profileSlice.actions;

export const User = (state) => state.profile.user;
export const IsConnected = (state) => state.profile.isConnected;
export const Error = (state) => state.profile.errorInitProfile;

export default profileSlice.reducer;
