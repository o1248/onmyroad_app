import React from 'react';
import {
	BrowserRouter as Router,
	Routes,
	Route,
} from 'react-router-dom';

import App from './App';
import Home from './View/Home';
import Login from './View/Login';
import Register from './View/Register';
import Confirmation from './View/Confirmation';
import MessageStep from './View/MessageStep';

import { PrivateRoute } from './Components/PrivateRoute';
import { ForwardRoute } from './Components/ForwardRoute';

export default function RouteList() {
	return (
		<Router>
			<Routes>
				<Route exact path="/" element={<Home />} />
				<Route exact path="/" element={<PrivateRoute />}>
					<Route exact path="/user" element={<App />} />
				</Route>
				<Route exact path="/" element={<ForwardRoute />}>
					<Route exact path="/login" element={<Login />} />
					<Route exact path="/register" element={<Register />} />
					<Route exact path="/confirmation/:token" element={<Confirmation />} />
					<Route exact path="/step/:message" element={<MessageStep />} />
				</Route>
			</Routes>
		</Router>
	);
}
